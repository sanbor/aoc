import aoc2020.Day14
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day14Test {
    @Test
    fun test1() {
        assertEquals(
            165, Day14.algo(
                "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", listOf(
                    "mem[8] = 11",
                    "mem[7] = 101",
                    "mem[8] = 0"
                )
            ).values.sum()
        )
    }
    @Test
    fun test2() {
        assertEquals(
            124899984582, Day14.algo(
                "X100110110X011000101000101XX11001X11", listOf(
                    "mem[5201] = 1838761",
                    "mem[32099] = 25747352",
                    "mem[36565] = 72187",
                    "mem[31494] = 369864",
                    "mem[17260] = 3138",
                    "mem[64903] = 91484814"
                )
            ).values.sum()
        )
    }

    @Test
    fun bits2int() {
        assertEquals(
            1838761, Day14.bits2int("000000000000000111000000111010101001")
            )
    }


    @Test
    fun bits2int2() {
        assertEquals(
            68719476735, Day14.bits2int("111111111111111111111111111111111111")
        )
    }

    @Test
    fun int2bits() {
        assertEquals(
              "000000000000000111000000111010101001", Day14.int2bits(1838761)
        )
    }


    @Test
    fun int2bits2() {
        assertEquals(
            "111111111111111111111111111111111111", Day14.int2bits(68719476735)
        )
    }



    @Test
    fun interpolate() {
        assertEquals(
            "000000000000000000000000000000000011", Day14.interpolate("00000000000000000000000000000000001X", "000000000000000111000000111010101001")
        )
        assertEquals(
            "000000000000000000000000000000000010", Day14.interpolate("00000000000000000000000000000000001X", "000000000000000111000000111010101000")
        )
        assertEquals(
            "000000000000000000000000000000000011", Day14.interpolate("00000000000000000000000000000000001X", "000000000000000111000000111010101001")
        )
        assertEquals(
            "100000000000000000000000000000000010", Day14.interpolate("10000000000000000000000000000000001X", "000000000000000111000000111010101000")
        )
    }

}
