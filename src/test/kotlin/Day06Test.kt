import aoc2020.Day06
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day06Test {
    @Test
    fun test1() {
        assertEquals(11, Day06.algo(listOf("abc", "abc", "abac", "aaaa", "b")))
    }
    @Test
    fun test2() {
        assertEquals(6, Day06.algo2(listOf(
            "abc",
            "a\nb\nc",
            "ab\nac",
            "a\na\na\na",
            "b")))
    }
}
