import aoc2020.Day16
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day16Test {
    @Test
    fun test1() {
        assertEquals(
            71, Day16.algo(
                listOf(
                    "class: 1-3 or 5-7",
                    "row: 6-11 or 33-44",
                    "seat: 13-40 or 45-50"
                ),
                listOf(
                    "7,3,47",
                    "40,4,50",
                    "55,2,20",
                    "38,6,12"
                )
            )
        )
    }
}
