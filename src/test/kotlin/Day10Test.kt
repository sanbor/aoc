import aoc2020.Day10
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day10Test {
    @Test
    fun test1() {
        assertEquals(
            7 * 5, Day10.algo(
                input
            )
        )
    }

    @Test
    fun test2() {
        assertEquals(
            22 * 10, Day10.algo(
                longInput
            )
        )
    }

    @Test
    fun test3() {
        assertEquals(
            8, Day10.algo3(
                input
            )
        )
    }

    @Test
    fun test4() {
        assertEquals(
            19208, Day10.algo3(
                longInput
            )
        )
    }

    private val input: List<Long> = listOf(
        16,
        10,
        15,
        5,
        1,
        11,
        7,
        19,
        6,
        12,
        4
    )

    private val longInput: List<Long> = listOf(
        28,
        33,
        18,
        42,
        31,
        14,
        46,
        20,
        48,
        47,
        24,
        23,
        49,
        45,
        19,
        38,
        39,
        11,
        1,
        32,
        25,
        35,
        8,
        17,
        7,
        9,
        4,
        2,
        34,
        10,
        3
    )
}
