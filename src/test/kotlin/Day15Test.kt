import aoc2020.Day15
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day15Test {
    @Test
    fun test1() {
        assertEquals(436L, Day15.algo(listOf(0, 3, 6)))
    }

    @Test
    fun test2() {
        assertEquals(1, Day15.algo(listOf(1, 3, 2)))
    }

    @Test
    fun test3() {
        assertEquals(10, Day15.algo(listOf(2, 1, 3)))
    }


    @Test
    fun test4() {
        assertEquals(27, Day15.algo(listOf(1, 2, 3)))
    }


    @Test
    fun test5() {
        assertEquals(78, Day15.algo(listOf(2, 3, 1)))
    }


    @Test
    fun test6() {
        assertEquals(438, Day15.algo(listOf(3, 2, 1)))
    }


    @Test
    fun test7() {
        assertEquals(1836, Day15.algo(listOf(3, 1, 2)))
    }



    @Test
    fun testPart21() {
        assertEquals(175594, Day15.algo(listOf(0, 3, 6), 30000000))
    }
}
