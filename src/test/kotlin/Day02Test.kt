import aoc2020.Day02
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class Day02Test {
    @Test
    fun test1() {
        assertEquals(
            Day02.algo(listOf(
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc"
        )), 2)
    }
    @Test
    fun test2() {
        assertEquals(
            Day02.algo2(listOf(
                "1-3 a: abcde",
                "1-3 b: cdefg",
                "2-9 c: ccccccccc"
            )), 1)
    }
}
