import aoc2020.Day12
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day12Test {
    @Test
    fun test1() {
        assertEquals(
            25, Day12.algo(
                listOf(
                    "F10",
                    "N3",
                    "F7",
                    "R90",
                    "F11"
                )
            )
        )
    }
}
