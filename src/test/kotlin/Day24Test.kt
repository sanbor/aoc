import aoc2020.Day24
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day24Test {
    @Test
    fun test0() {
        assertEquals(
            1, Day24.algo(
                listOf(
                    "wewe"
                )
            )
        )
    }

    @Test
    fun test3() {
        assertEquals(
            1, Day24.algo(
                listOf(
                    "nwwswee",
                    "nwwswee",
                    "nwwswee"
                )
            )
        )
    }
    @Test
    fun test1() {
        assertEquals(
            10, Day24.algo(
                listOf(
                    "sesenwnenenewseeswwswswwnenewsewsw",
                    "neeenesenwnwwswnenewnwwsewnenwseswesw",
                    "seswneswswsenwwnwse",
                    "nwnwneseeswswnenewneswwnewseswneseene",
                    "swweswneswnenwsewnwneneseenw",
                    "eesenwseswswnenwswnwnwsewwnwsene",
                    "sewnenenenesenwsewnenwwwse",
                    "wenwwweseeeweswwwnwwe",
                    "wsweesenenewnwwnwsenewsenwwsesesenwne",
                    "neeswseenwwswnwswswnw",
                    "nenwswwsewswnenenewsenwsenwnesesenew",
                    "enewnwewneswsewnwswenweswnenwsenwsw",
                    "sweneswneswneneenwnewenewwneswswnese",
                    "swwesenesewenwneswnwwneseswwne",
                    "enesenwswwswneneswsenwnewswseenwsese",
                    "wnwnesenesenenwwnenwsewesewsesesew",
                    "nenewswnwewswnenesenwnesewesw",
                    "eneswnwswnwsenenwnwnwwseeswneewsenese",
                    "neswnwewnwnwseenwseesewsenwsweewe",
                    "wseweeenwnesenwwwswnew"
                )
            )
        )
    }
}
