import aoc2020.Day25
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day25Test {
    @Test
    fun test1() {
        assertEquals(14897079, Day25.algo(5764801,17807724))
    }
}
