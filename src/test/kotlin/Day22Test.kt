import aoc2020.Day22
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day22Test {
    @Test
    fun test1() {
        assertEquals(
            306, Day22.algo(
                listOf(9, 2, 6, 3, 1),
                listOf(5, 8, 4, 7, 10)
            )
        )
    }
}
