import aoc2020.Day05
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day05Test {
    @Test
    fun test1() {
        assertEquals(567, Day05.algo(listOf("BFFFBBFRRR")))
        assertEquals(119, Day05.algo(listOf("FFFBBBFRRR")))
        assertEquals(820, Day05.algo(listOf("BBFFBBFRLL")))
        // Verify max is working
        assertEquals(820, Day05.algo(listOf("BBFFBBFRLL", "BFFFBBFRRR")))
        assertEquals(820, Day05.algo(listOf("BFFFBBFRRR", "BBFFBBFRLL")))
    }
}
