import aoc2020.Day18
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day18Test {
    @Test
    fun testt1() {
        assertEquals(2, Day18.algo(listOf("1+1")))
        assertEquals(4, Day18.algo(listOf("2+2")))
        assertEquals(5, Day18.algo(listOf("2+3")))
        assertEquals(6, Day18.algo(listOf("2*3")))
        assertEquals(11, Day18.algo(listOf("2*3+5")))
        assertEquals(26, Day18.algo(listOf("2 * 3 + ( 4 * 5 )")))

        assertEquals(437, Day18.algo(listOf("5+(8*3+9+3*4*3)")))
        assertEquals(12240, Day18.algo(listOf("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")))
        assertEquals(13632, Day18.algo(listOf("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))
        assertEquals(12240 + 13632, Day18.algo(listOf("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))

    }
}
