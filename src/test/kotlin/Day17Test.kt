import aoc2020.Day17
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day17Test {
    @Test
    fun test1() {
        assertEquals(
            112, Day17.algo(
                listOf(
                    ".#.",
                    "..#",
                    "###"
                )
            )
        )
    }
}
