import aoc2020.Day09
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day09Test {
    @Test
    fun test1() {
        assertEquals(127, Day09.algo(input, 5))
    }

    @Test
    fun test2() {
        assertEquals(62, Day09.algo2(input, Day09.algo(input, 5)))
    }

    private val input: List<Long> = listOf(
        35,
        20,
        15,
        25,
        47,
        40,
        62,
        55,
        65,
        95,
        102,
        117,
        150,
        182,
        127,
        219,
        299,
        277,
        309,
        576
    )
}
