import aoc2020.Day08
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day08Test {
    @Test
    fun test1() {
        assertEquals(
            5, Day08.algo(
                listOf(
                    "nop +0",
                    "acc +1",
                    "jmp +4",
                    "acc +3",
                    "jmp -3",
                    "acc -99",
                    "acc +1",
                    "jmp -4",
                    "acc +6"
                )
            )
        )
    }

    @Test
    fun test2() {
        assertEquals(
            8, Day08.algo2(
                listOf(
                    "nop +0",
                    "acc +1",
                    "jmp +4",
                    "acc +3",
                    "jmp -3",
                    "acc -99",
                    "acc +1",
                    "jmp -4",
                    "acc +6"
                )
            )
        )
    }
}
