import aoc2020.Day19
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day19Test {
    @Test
    fun test1() {
        assertEquals(
            2, Day19.algo(
                listOf(
                    "0: 4 1 5",
                    "1: 2 3 | 3 2",
                    "2: 4 4 | 5 5",
                    "3: 4 5 | 5 4",
                    "4: \"a\"",
                    "5: \"b\""
                ), listOf(
                    "ababbb",
                    "bababa",
                    "abbbab",
                    "aaabbbv",
                    "aaaabbb"
                )
            )
        )
    }
}
