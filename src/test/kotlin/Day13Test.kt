import aoc2020.Day13
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day13Test {
    @Test
    fun test1() {
        assertEquals(295, Day13.algo(939,"7,13,x,x,59,x,31,19".split(",")))
    }

    @Test
    fun test2() {
        assertEquals(1068781, Day13.algo2("7,13,x,x,59,x,31,19".split(",")))
    }

    @Test
    fun test3() {
        assertEquals(1068781, Day13.algo2("7,13,x,x,59,x,31,19".split(",")))
    }
}
