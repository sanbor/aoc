import aoc2020.Day23
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Day23Test {
    @Test
    fun test1() {
        assertEquals("92658374", Day23.algo(listOf(3,8,9,1,2,5,4,6,7), 10))
        assertEquals("67384529", Day23.algo(listOf(3,8,9,1,2,5,4,6,7), 100))
    }
}
