package aoc2020

import kotlin.math.absoluteValue

object Day12 {
    fun algo(input: List<String>): Long {
        var east: Long = 0
        var north: Long = 0
        var direction = 0

        for (dir in input) {
            val action = dir.substring(0, 1)
            val value = dir.substring(1).toLong()
            when (action) {
                "N" -> {
                    north += value
                }
                "S" -> {
                    north -= value
                }
                "E" -> {
                    east += value
                }
                "W" -> {
                    east -= value
                }
                "L" -> {
                    direction -= value.toInt()
                }
                "R" -> {
                    direction += value.toInt()
                }
                "F" -> {
                    when (direction) {
                        0 -> {
                            east += value
                        }
                        -270, 90 -> {
                            north -= value
                        }
                        -180, 180 -> {
                            east -= value
                        }
                        -90, 270 -> {
                            north += value
                        }
                    }

                }

            }

            direction %= 360
            println("action: $dir\neast: $east\nnorth:$north\ndirection:$direction\n---")

        }

        return east.absoluteValue + north.absoluteValue
    }
}

fun main() {
    println(Day12.algo(Utils.getDayLines("12")))
}
