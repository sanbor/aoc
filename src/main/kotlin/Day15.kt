package aoc2020

object Day15 {
    fun algo(input: List<Long>, nth: Int = 2020): Long {
        val turns = mutableMapOf<Long, MutableList<Int>>()

        var last:Long = input.last()
        turns[0] = mutableListOf(input.size)
        for (i in 0 until nth) {
            if (i < input.size) {
                turns[input[i]] = mutableListOf(i)
            } else {
                if (turns[last]!!.size == 1) {
                    turns[0]?.add(i)
                    last = 0
                } else {
                    val two = turns[last]!!.takeLast(2)
                    last = (two.last() - two.first()).toLong()

                    if (turns[last] == null) {
                        turns[last] = mutableListOf(i)
                    }else {
                        turns[last]?.add(i)
                    }
                }
            }

        }

        return last
    }
}

fun main() {
    println(Day15.algo(listOf(1,0,16,5,17,4)))
    println(Day15.algo(listOf(1,0,16,5,17,4), 30000000))
}
