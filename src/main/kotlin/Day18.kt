package aoc2020

object Day18 {
    fun algo(input: List<String>): Long {
        return input.map { calc(it) }.sum()
    }

    private fun calc(str: String): Long {
        var sum: Long = 0
        var operation: String = "+"

        for (digit in breakup(str.filter { it.toString().isNotBlank() }).withIndex()) {
            if ("(" in digit.value) {
                val result = calc(digit.value.substring(1, digit.value.length - 1))
                if (operation == "+") {
                    sum += result
                } else if (operation == "*") {
                    sum *= result
                }
            } else if (digit.value in "+*") {
                operation = digit.value
            } else if (digit.value.any { it.isDigit() }) {
                if (operation == "+") {
                    sum += digit.value.toLong()
                } else if (operation == "*") {
                    sum *= digit.value.toLong()
                }
            }
        }

        return sum
    }

    private fun breakup(str: String): List<String> {
        var result = mutableListOf<String>()
        var skipCounter = 0
        var skipRange = IntRange(0, 0)
        for ((index, value) in str.withIndex()) {
            if (value in "(") {
                if (skipCounter == 0) {
                    skipRange = IntRange(index, skipRange.last)
                }

                skipCounter++

            } else if (value in ")") {
                skipCounter--
                if (skipCounter == 0) {
                    skipRange = IntRange(skipRange.first, index)
                    result.add(str.substring(skipRange))
                }
            } else if (skipCounter == 0) {
                result.add(value.toString())
            }
        }

        return result
    }
}

fun main() {
    println(Day18.algo(Utils.getDayLines("18")))
}
