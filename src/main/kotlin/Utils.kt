package aoc2020

import java.io.File

object Utils {
    fun getDayLines(day: String): List<String> = File("src/main/resources/Day$day.txt").readLines()
}
