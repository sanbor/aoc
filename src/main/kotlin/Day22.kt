package aoc2020

object Day22 {
    fun algo(player1: List<Int>, player2: List<Int>): Long {
        var p1 = player1.toMutableList()
        var p2 = player2.toMutableList()

        while (p1.isNotEmpty() && p2.isNotEmpty()) {
            if (p1.first() > p2.first()) {
                p1.add(p1.size, p1.first())
                p1.add(p1.size, p2.first())
                p1.removeAt(0)
                p2.removeAt(0)
            } else {
                p2.add(p2.size, p2.first())
                p2.add(p2.size, p1.first())
                p2.removeAt(0)
                p1.removeAt(0)
            }
        }

        return (p1 + p2).reversed().withIndex().map { (index, value) -> (index + 1) * value }.sum().toLong()
    }
}

fun main() {
    println(
        Day22.algo(
            listOf(
                29,
                21,
                38,
                30,
                25,
                7,
                2,
                36,
                16,
                44,
                20,
                12,
                45,
                4,
                31,
                34,
                33,
                42,
                50,
                14,
                39,
                37,
                11,
                43,
                18
            ), listOf(
                32,
                24,
                10,
                41,
                13,
                3,
                6,
                5,
                9,
                8,
                48,
                49,
                46,
                17,
                22,
                35,
                1,
                19,
                23,
                28,
                40,
                26,
                47,
                15,
                27
            )
        )
    )
}
