package aoc2020

object Day23 {
    fun algo(input: List<Int>, times: Int): String {
        var cups = input.toMutableList()
        var pickup = mutableListOf<Int>()
        var currentIndex = 0
        var destination = 0

        for (i in 1..times) {
            println("-- move $i --")
            println("cups: ${cups.map { if (it == cups[currentIndex]) { "($it)" }else {"$it"} }}")

            // pickup three cups
            when (currentIndex) {
                in 0..cups.size - 4 -> pickup = cups.subList(currentIndex + 1, currentIndex + 4)
                cups.size - 3 -> pickup = (cups.subList(currentIndex + 1, currentIndex + 3) + mutableListOf(cups.first())) as MutableList<Int>
                cups.size - 2 -> pickup = (mutableListOf(cups.last()) + cups.subList(0, 2)) as MutableList<Int>
                cups.size - 1 -> pickup = cups.subList(0, 3)
            }

            println("pick up: $pickup")

            val currentCup = cups[currentIndex]
            cups = (cups - pickup) as MutableList<Int>

            // set destination
            destination = findDestinationCup(currentCup, cups, pickup)
            println("destination: $destination")

            // insert pickup after destination
            val destinationIndex = cups.indexOf(destination)
            cups = (cups.subList(0, destinationIndex + 1) + pickup + cups.subList(destinationIndex + 1, cups.size)) as MutableList<Int>

            // Rebalance so currentIndex stays in the same location as before inserting
            val newIndex = cups.indexOf(currentCup)
            if (newIndex > currentIndex) {
                cups = (cups.subList(newIndex - currentIndex, cups.size) + cups.subList(0, newIndex - currentIndex)) as MutableList<Int>
            } else if (newIndex < currentIndex) {
                cups = (cups.subList(currentIndex - newIndex, cups.size) + cups.subList(0, currentIndex - newIndex)) as MutableList<Int>
            }

            // set new current
            currentIndex = if (currentIndex < cups.size - 1) currentIndex + 1 else 0

        }

        println("-- final --")
        println("cups: ${cups.map { if (it == cups[currentIndex]) { "($it)" }else {"$it"} }}")

        val oneIndex = cups.indexOf(1)
        val result = (cups.subList(oneIndex + 1, cups.size) + cups.subList(0, oneIndex)) as MutableList<Int>
        return result.joinToString("", "")
    }

    private fun findDestinationCup(currentCup: Int, cups: MutableList<Int>, pickup: MutableList<Int>): Int {
        var destination: Int = currentCup - 1

        destination = if (destination < cups.minOf { it }) {
            cups.maxOf { it }
        } else {
            cups.sorted().reversed().first { it <= destination }
        }

        return destination
    }
}

fun main() {
    println(Day23.algo(listOf(7, 9, 2, 8, 4, 5, 1, 3, 6), 100))
}
