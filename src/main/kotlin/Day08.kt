package aoc2020

import java.io.File
import kotlin.random.Random

enum class Operation {
    acc,jmp,nop
}
data class Instruction(val name:Operation, val arg:Int, val ran:Boolean = false)

object Day08 {
    fun algo(input: List<String>):Int {
        var pc = 0
        var acc = 0

        val program = input.map {
            val (instruction, arg) = it.split(" ")
            Instruction(Operation.valueOf(instruction), arg.toInt())
        }.toMutableList()

        var secondRun = false
        var instruction = program[pc]

        while (!secondRun) {
            program[pc] = program[pc].copy(ran = true)
            when (instruction.name) {
                Operation.acc -> {
                    acc += instruction.arg
                    pc++
                }
                Operation.nop -> {
                    pc++
                }
                Operation.jmp -> {
                    pc += instruction.arg
                }
            }

            instruction = program[pc]

            if (instruction.ran) {
                secondRun = true
            }
        }
        return acc
    }

    fun algo2(input: List<String>):Int {
        var pc = 0
        var acc = 0

        val program = input.map {
            val (instruction, arg) = it.split(" ")
            Instruction(Operation.valueOf(instruction), arg.toInt())
        }.toMutableList().filterNot { it.name == Operation.nop && it.arg == 0 }

        var lastMutation = 0
        var mutantProgram = ArrayList(program)
        var instruction = mutantProgram[pc]

        while (pc < program.size) {
            mutantProgram[pc] = mutantProgram[pc].copy(ran = true)
            when (instruction.name) {
                Operation.acc -> {
                    acc += instruction.arg
                    pc++
                }
                Operation.nop -> {
                    pc++
                }
                Operation.jmp -> {
                    pc += instruction.arg
                }
            }

            if (pc < mutantProgram.size) {
                instruction = mutantProgram[pc]
                if (instruction.ran) {
                    pc = 0
                    acc = 0
                    mutantProgram = ArrayList(program)

                    val matches = mutantProgram.filter { it.name == Operation.nop || it.name == Operation.jmp }.map { mutantProgram.indexOf(it) }

                    val mutationIndex = matches.subList(lastMutation, matches.size)[0]
                    lastMutation++

                    if (program[mutationIndex].name == Operation.nop) {
                        mutantProgram[mutationIndex] = mutantProgram[mutationIndex].copy(name = Operation.jmp)
                    } else {
                        mutantProgram[mutationIndex] = mutantProgram[mutationIndex].copy(name = Operation.nop)
                    }

                    instruction = mutantProgram[pc]
                }
            }
        }
        return acc
    }
}

fun main() {
    println(Day08.algo(File("src/main/resources/Day08.txt").readLines())) // 1782
    println(Day08.algo2(File("src/main/resources/Day08.txt").readLines())) // 797
}
