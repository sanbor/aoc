package aoc2020

object Day10 {
    fun algo(input: List<Long>): Int {
        val deviceJolt = input.maxOrNull()!! + 3
        val adapters = (input + deviceJolt).sorted()
        var prev = 0L
        val diffs = mutableListOf<Long>()
        for (current in adapters) {
            diffs.add(current - prev)
            prev = current
        }
        return diffs.count { it == 1L } * diffs.count { it == 3L }
    }

    fun algo2(input: List<Long>): Long {
        val deviceJolt: Long = input.maxOrNull()!! + 3
        val adapters = (input + 0L + deviceJolt).sorted()
        var prev: Long = 0L
        val diffs = mutableListOf<Long>()
        val compacted = mutableListOf<Long>(0)
        if (adapters[3] == 3L) {
            compacted.add(3L)
        }
        for ((index, current) in adapters.withIndex()) {
            if (index != 0) {
                if (index > 2 && ((current - adapters[index - 1]) + (adapters[index - 1] - adapters[index - 2]) + (adapters[index - 2] - adapters[index - 3]) == 3L)) {
                    continue
                } else if (index < 3 && adapters[3] == 3L) {
                    continue
                } else {
                    compacted.add(current)
                }

                diffs.add(current - prev)
                prev = current
            }
        }
        return (diffs.size - compacted.size).toLong()
    }

    fun algo3(input: List<Long>): Long {
        val deviceJolt = input.maxOrNull()!! + 3
        var adapters = (input + deviceJolt).sorted()
        var prev = 0L
        val diffs = mutableListOf<Long>()
        for (current in adapters) {
            if (current == 0L) {
                continue
            }
            diffs.add(current - prev)
            prev = current
        }
        val compacted = mutableListOf<Long>()
        for ((index, current) in diffs.withIndex()) {
            if (current == 1L && diffs[index + 1] == 1L && diffs[index + 2] == 1L) {
                continue
            } else if (index > 1 && diffs[index -2] == 3L && diffs[index -1] == 1L && current == 1L && diffs[index + 1] == 1L) {
                continue

            } else if (index > 0 && diffs[index -1] == 3L && current == 1L && diffs[index + 1] == 1L) {
                continue

            } else {
                compacted.add(adapters[index])
            }
        }

        return factorial((diffs.size - compacted.size).toLong()) + 2L
    }

    private fun factorial(i: Long): Long {
        if (i == 0L) {
            return 1L
        }
        return i * factorial(i - 1)
    }
}

fun main() {
    println(Day10.algo(Utils.getDayLines("10").map { it.toLong() }))
    println(Day10.algo3(Utils.getDayLines("10").map { it.toLong() }))
}
