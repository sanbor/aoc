package aoc2020

object Day13 {
    fun algo(earliest: Long, buses: List<String>): Long {
        var time = earliest
        var timetable = mutableListOf<Pair<Int, Long>>()
        while (timetable.isEmpty()) {
            for(bus in buses.filterNot { it == "x" }) {
                val b = bus.toInt()
                if (time % b == 0L) {
                    timetable.add(b to time)
                }
            }
            time++
        }

        return timetable.sortedWith(compareBy { (_, time) -> time }).first().let { (bus: Int, time: Long) -> bus * (time - earliest) }
    }

    fun algo2(buses: List<String>): Long {
        var time:Long = 106880L
        var timetable = mutableListOf<Pair<Int, Long>>()
        val ignoreX = buses.filterNot { it == "x" }
        while (timetable.map { it.first }.distinct().size < ignoreX.size) {
            for((index, bus) in buses.withIndex()) {
                if (bus == "x") {
                    continue
                }
                val b = bus.toInt()


                if (time % b == 0L) {
                    if (index < 1 || timetable.size < 1 || (time - index == timetable.first { it.first == ignoreX.first().toInt() }.second)) {
                        timetable.add(b to time)
                    }
                }
            }
            time++
        }

        return timetable.sortedWith(compareBy { (_, time) -> time }).first().let { (bus: Int, time: Long) -> time }
    }
}

fun main() {
    val buses =
        "29,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,631,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,19,x,x,x,23,x,x,x,x,x,x,x,383,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,17".split(
            ","
        )
    println("Day13 - Part 1:" +
        Day13.algo(
            1000507,
            buses
        )
    )

    println("Day13 - Part 2:" +
        Day13.algo2(
            buses
        )
    )
}
