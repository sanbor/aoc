package aoc2020

object Day19 {
    fun algo(rawRules: List<String>, input: List<String>): Long {
        var counter = 0L
        var rules = mutableListOf<String>()

        for (rule in rawRules.sortedBy { it.split(": ")[0].toInt() }) {
            val (i, value) = rule.split(": ")
            rules.add(i.toInt(), value)
        }

        while (rules[0].any { it.isDigit() }) {
            rules[0] = rules[0].split(" ").map {

                if (it.any { ch -> ch.isDigit() }) {
                    val length = rules[it.toInt()].length
                    val r = rules[it.toInt()]
                    if ("\"" in r) {
                        r.substring(1, length - 1)
                    } else {
                        " ( $r ) "
                    }
                } else {
                    it
                }
            }.joinToString(" ")
        }


        for (value in input) {
            if (rules[0].replace(" ", "").toRegex().matches(value)) {
                counter++
            }
        }

        return counter
    }

}


fun main() {
    val lines: List<String> = Utils.getDayLines("19")
    val blank = lines.indexOfFirst { it.isBlank() }
    val rules = lines.subList(0, blank)
    val input = lines.subList(blank + 1, lines.size)

    println(Day19.algo(rules, input))
}
