package aoc2020

import java.io.File

object Day09 {
    fun algo(input: List<Long>, preambleLength: Int):Long {
        var pStart = 0
        var pEnd = preambleLength

        for (value in input.subList(preambleLength, input.size)) {
            val preamble = input.subList(pStart, pEnd)
            pStart++
            pEnd++

            if (!isSumOf(value, preamble)) {
                return value
            }
        }

        return -1
    }

    private fun isSumOf(value:Long, preamble: List<Long>): Boolean {
        for(a in preamble) {
            for(b in preamble.minus(a)) {
                if (a + b == value) {
                    return true
                }
            }
        }

        return false
    }
    fun algo2(input: List<Long>, number: Long):Long {
        var sum = 0L
        var start = 0
        var end = 0

        while(sum != number) {
            sum = 0

            for ((index, value) in input.subList(start++, input.size).withIndex()) {
                sum += value
                end = index
                if (sum == number) {
                    break
                }
            }
        }

        val numbers = input.subList((start - 1), start + (end + 1))
        return numbers.minOrNull()!! + numbers.maxOrNull()!!
    }
}

fun main() {
    val input = File("src/main/resources/Day09.txt").readLines().map { it.toLong() }
    println(Day09.algo(input, 25)) // 50047984
    println(Day09.algo2(input, Day09.algo(input, 25))) // 5407707
}
