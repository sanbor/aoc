package aoc2020

import java.io.File

data class Bag(
    val color: String,
    val bags: List<Bag>
)

object Day07 {
    private fun findColor(bag: Bag, color: String): Boolean = if (bag.color == color) {
        true
    } else {
        bag.bags.any { findColor(it, color) }
    }

    fun algo(input: List<String>): Int {
        val lookupColor = "shiny gold"
        val delimiter = "bags contain "
        var bags = mutableMapOf<String, Bag>()
        for (line in input) {
            val spaceSplit = line.replace(",", "").replace(".", "").split(' ')
            val currentColor = "${spaceSplit[0]} ${spaceSplit[1]}"
            bags[currentColor] = Bag(color = currentColor, bags = emptyList())
        }


        for (line in input) {
            val spaceSplit = line.replace(",", "").replace(".", "").replace(".", "").split(' ')
            val currentColor = "${spaceSplit[0]} ${spaceSplit[1]}"
            val separator = line.indexOf(delimiter) + delimiter.length
            val contains = line.substring(separator).replace("bags", "").replace("bag", "").replace(".", "").split(", ").chunked(3).flatten()
                .map { val s = it.split(' '); Bag(color = "${s[1]} ${s[2]}", bags = emptyList()) }

            if ("no" !in spaceSplit) {
                bags[currentColor] = bags[currentColor]!!.copy(bags = contains)
            }
        }
        for (i in 1..100) {
            for ((currentColor, bag) in bags) {
                val b: List<Bag> = bag.bags.map { bags[it.color]!! }
                bags[currentColor] = bag.copy(bags = b)
            }
        }

        return bags.filterNot { it.key == lookupColor }.count { findColor(it.value, lookupColor) }
    }
}

fun main() {
    println(Day07.algo(File("src/main/resources/Day07.txt").readLines())) //172
}
