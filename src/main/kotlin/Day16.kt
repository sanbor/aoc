package aoc2020

object Day16 {
    fun algo(fieldConstraints: List<String>, input: List<String>): Long {
        val constraints = fieldConstraints.map {
            """(\w+?)(?<num>\d+)""".toRegex().matchEntire("area51")!!.groups["num"]!!.value
            // departure location: 43-237 or 251-961
            val results: List<String> = """(\D+):\s(\d+)-(\d+)\sor\s(\d+)-(\d+)""".toRegex().matchEntire(it)!!.groupValues

            val label = results[1]
            val low1 = results[2].toInt()
            val low2 = results[3].toInt()
            val high1 = results[4].toInt()
            val high2 = results[5].toInt()

            Pair(IntRange(low1, low2), IntRange(high1, high2))
        }

        val errors = mutableListOf<Long>()
        for (value in input) {
            value.split(",").forEach { cell: String ->
                val l = cell.toLong()

                if (constraints.none { constraint ->
                        constraint.first.contains(l) || constraint.second.contains(l)
                    }
                ) {
                    errors.add(l)
                }
            }
        }


        return errors.sum()
    }
}

fun main() {
    println(
        Day16.algo(
            listOf(
                "departure location: 43-237 or 251-961",
                "departure station: 27-579 or 586-953",
                "departure platform: 31-587 or 608-967",
                "departure track: 26-773 or 784-973",
                "departure date: 41-532 or 552-956",
                "departure time: 33-322 or 333-972",
                "arrival location: 30-165 or 178-965",
                "arrival station: 31-565 or 571-968",
                "arrival platform: 36-453 or 473-963",
                "arrival track: 35-912 or 924-951",
                "class: 39-376 or 396-968",
                "duration: 31-686 or 697-974",
                "price: 28-78 or 96-971",
                "route: 32-929 or 943-955",
                "row: 40-885 or 896-968",
                "seat: 26-744 or 765-967",
                "train: 46-721 or 741-969",
                "type: 30-626 or 641-965",
                "wagon: 48-488 or 513-971",
                "zone: 34-354 or 361-973"
            ),
            Utils.getDayLines("16")
        ))
}
