package aoc2020

import kotlin.math.pow

object Day14 {
    fun algo(mask: String, instructions: List<String>): Map<Int, Long> {
        var data = mutableListOf<Pair<Int, Long>>()

        for (inst in instructions) {
            val reg = inst.replace("mem[", "").split("]")[0]
            val v = inst.split("= ")[1]
            data.add(reg.toInt() to v.toLong())
        }

        val results = mutableMapOf<Int, Long>()
        for((k, v) in data) {
            results[k] =bits2int(interpolate(mask, int2bits(v)))
        }

        return results
    }

    fun int2bits(n:Long):String {
        var result = "000000000000000000000000000000000000"

        var d:Long = n
        for(i in (0..35).reversed()) {

            val r = (d % 2)
            d /= 2

            result = result.replaceRange(i, i +1, r.toString())
         }
        return result
    }

    fun bits2int(bits:String):Long {
        var sum = 0.0


        for((i, c) in bits.reversed().withIndex()) {
            sum += 2.0.pow(i) * c.toString().toInt()
        }

        return sum.toLong()
    }

    fun interpolate(mask: String, bits: String): String {
        return String(mask.mapIndexed { index, c -> if (c == 'X') bits[index] else c }.toCharArray())
    }
}

fun main() {
    val program = Utils.getDayLines("14")
    val l = mutableMapOf<String, MutableList<String>>()
    var currentMask = ""
    for (p in program) {
        if ("mask" in p) {
            currentMask = p.substring(7)
            l[currentMask] = mutableListOf()
        } else {
            l[currentMask]?.add(p)
        }
    }

    var a = mutableMapOf<Int, Long>()

    for (k in l.keys) {
        for ((address, value) in Day14.algo(k, l[k]!!)) {
            a[address] = value
        }
    }
    println(a.values.sum())
}
