package aoc2020

object Day11 {
    fun algo(input: List<String>): Long {
        var currentMap = mutableListOf<String>()
        var previousMap = input.toMutableList()

        while (currentMap != previousMap) {
            if (currentMap.isNotEmpty()) {
                previousMap = currentMap.toMutableList()
            } else {
                currentMap = previousMap.toMutableList()
            }

            for ((i, row) in previousMap.withIndex()) {
                println(row)

                for ((j, cell) in row.withIndex()) {
                    if (cell == 'L') {
                        val canBeOccupied = checkState(previousMap, i, j, 'L')
                        if (canBeOccupied) {
                            currentMap[i] = currentMap[i].replaceRange(j, j + 1, "#")
                        }
                    } else if (cell == '#') {
                        if (canBeEmptied(previousMap, i, j, '#', 4)) {
                            currentMap[i] = currentMap[i].replaceRange(j, j + 1, "L")
                        }
                    }
                }
            }
            println("\n\n\n---\n\n\n")
        }

        return currentMap.sumBy { row -> row.count { seat -> seat == '#' } }.toLong()
    }

    private fun checkState(map: List<String>, i: Int, j: Int, status: Char): Boolean {
        val topLimit = i < 1
        val leftLimit = j < 1
        val rightLimit = j + 1 == map[i].length
        val bottomLimit = i + 1 == map.size
        return map[i][j] == status // current
                && (topLimit || map[i - 1][j] == status || map[i - 1][j] == '.') // top
                && (bottomLimit || map[i + 1][j] == status || map[i + 1][j] == '.') // bottom
                && (leftLimit || map[i][j - 1] == status || map[i][j - 1] == '.') // left
                && (rightLimit || map[i][j + 1] == status || map[i][j + 1] == '.') // right
                && ((topLimit || leftLimit) || map[i - 1][j - 1] == status || map[i - 1][j - 1] == '.') // top-left
                && ((topLimit || rightLimit) || map[i - 1][j + 1] == status || map[i - 1][j + 1] == '.') // top-right
                && ((bottomLimit || leftLimit) || map[i + 1][j - 1] == status || map[i + 1][j - 1] == '.') // bottom-left
                && ((bottomLimit || rightLimit) || map[i + 1][j + 1] == status || map[i + 1][j + 1] == '.') // bottom-right
    }

    private fun canBeEmptied(map: List<String>, i: Int, j: Int, status: Char, threadshold: Int): Boolean {
        val topLimit = i < 1
        val leftLimit = j < 1
        val rightLimit = j + 1 == map[i].length
        val bottomLimit = i + 1 == map.size
        val current = map[i][j] == status
        val top = !topLimit && map[i - 1][j] == status
        val bottom = !bottomLimit && map[i + 1][j] == status
        val left = !leftLimit && map[i][j - 1] == status
        val right = !rightLimit && map[i][j + 1] == status
        val topLeft = !(topLimit || leftLimit) && map[i - 1][j - 1] == status
        val topRight = !(topLimit || rightLimit) && map[i - 1][j + 1] == status
        val bottomLeft = !(bottomLimit || leftLimit) && map[i + 1][j - 1] == status
        val bottomRight = !(bottomLimit || rightLimit) && map[i + 1][j + 1] == status

        return current && listOf(top, bottom, left, right, topLeft, topRight, bottomLeft, bottomRight).count { it } >= threadshold
    }

}

fun main() {
    println(Day11.algo(Utils.getDayLines("11"))) // 2319
}
