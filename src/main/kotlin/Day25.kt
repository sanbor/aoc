package aoc2020

import kotlin.math.ceil
import kotlin.math.sqrt
import java.math.BigInteger.valueOf as bigInt

object Day25 {
    const val xmasPrime = 20201227L
    fun algo(cardPk: Long, doorPk: Long): Long {
        // find card loop size
        val x = babyStepGiantStep(7, cardPk)

        // g.modPow(x, p) = g^x % p
        return bigInt(doorPk).modPow(bigInt(x), bigInt(xmasPrime)).toLong()
    }

    // Baby step, giant step implementation borrowed from https://gist.github.com/0xTowel/b4e7233fc86d8bb49698e4f1318a5a73
    // after failed attempts trying to implement https://www.johndcook.com/blog/2016/10/21/computing-discrete-logarithms-with-baby-step-giant-step-algorithm/
    // and https://orion.math.iastate.edu/cbergman/crypto/psfiles/4up/discretelogs.pdf

    // Solve for x in b = g^x % p given a prime p. using Shanks' Algorithm aka baby step, giant step
    fun babyStepGiantStep(g: Long = 7, b: Long, prime: Long = xmasPrime): Long {
        val m = ceil(sqrt((prime - 1).toDouble())).toLong()
        val n = bigInt(prime)

        // Store map of g^{1...m} (mod p). Baby step.
        val baby = (0 until m).map { bigInt(g).modPow(bigInt(it), n) to it }

        // Search for an equivalence in the table. Giant step.
        for (j in 0 until m) {
            // compute b*g^(−j) % n
            val gPowNegJ = bigInt(g).modPow(bigInt(m * (prime - 2)), n).modPow(bigInt(j), n)
            val giant = bigInt(b).times(gPowNegJ).mod(n)

            // look for b*g^(−i) % n in baby list
            val match = baby.find { it.first == giant }
            if (match != null) {
                // x = mj + baby[i]
                return (m * j + match.second)
            }
        }

        throw Exception("Not found, probably invalid prime argument")
    }
}

fun main() {
    // Example
    println(Day25.algo(5764801, 17807724)) // 14897079

    // Puzzle input
    println(Day25.algo(12578151, 5051300)) // 296776
}
