package aoc2020

object Day20 {
    fun algo(input: List<String>): Long {
        var tiles = mutableMapOf<Int, List<String>>()

        for ((index, value) in input.withIndex()) {
            if ("Tile" in value) {
                val id = value.substring(5, value.length -1).toInt()
                tiles[id] = input.subList(index + 1, index + 11)
            }
        }

        var edges = mutableMapOf<Int, Int>()
        for ((id, tile) in tiles) {
            edges[id] = (edges[id] ?: 0)  + countMatches(tile, tiles - id)
        }

        return edges.toList().sortedBy { it.second }.take(4).map { it.first.toLong() }.reduce{ sum, el -> sum!! * el!! }!!
    }

    private fun countMatches(current: List<String>, tiles: Map<Int, List<String>>): Int {
        var counter = 0
        val sides = listOf(current.first(), current.map { it.last() }.joinToString("", ""), current.last(), current.map { it.first() }.joinToString("", ""))
        for ((id, tile) in tiles) {
            for (side in sides) {
                when (side) {
                    tile.first(),

                    flip(true, tile).first(),
                    flip(false, tile).first(),

                    rotate(1, tile).first(),
                    rotate(2, tile).first(),
                    rotate(3, tile).first(),

                    flip(true, rotate(1, tile)).first(),
                    flip(true, rotate(2, tile)).first(),
                    flip(true, rotate(3, tile)).first(),

                    flip(false, rotate(1, tile)).first(),
                    flip(false, rotate(2, tile)).first(),
                    flip(false, rotate(3, tile)).first() ->
                        counter++
                }
            }
        }


        return counter
    }

    private fun flip(horizontal: Boolean = true, input: List<String>): List<String> = if (horizontal) {
        input.map { it.reversed() }
    } else {
        input.reversed()
    }


    private fun rotate(times: Int, input: List<String>): List<String> {
        var rotated = input.toMutableList()
        for (rotation in 1..times) {
            rotated = rotated.indices.map { index ->
                rotated.toList().map {
                    it[index]
                }.reversed().joinToString("", "")
            }.toMutableList()
        }

        return rotated
    }
}

fun main() {
    println(Day20.algo(Utils.getDayLines("20")))
}
