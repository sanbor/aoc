package aoc2020

object Day21 {
    data class Allergen(
        val name: String,
        var ingredients: MutableMap<String, Int>
    )

    fun algo(input: List<String>): Long {
        var foreignLabels = mutableListOf<Pair<List<String>, List<String>>>()
        var ingredientsToAllergens = mutableMapOf<String, String?>()
        var allergens = mutableListOf<Allergen>()

        // Populate lists
        for (value in input) {
            var (rawIngredients, rawAlergens) = value.split(" (contains ")
            val ingredientList = rawIngredients.split(" ")
            val allergenList = rawAlergens.split(", ").map { it.replace(")", "") }

            allergenList.forEach { allergen ->
                if (allergens.none { it.name == allergen }) {
                    allergens.add(Allergen(allergen, mutableMapOf()))
                }

                ingredientList.map { ingredient ->
                    var w = allergens.single { it.name == allergen }
                    if (w.ingredients[ingredient] != null) {
                        allergens[allergens.indexOf(w)].ingredients[ingredient] = allergens[allergens.indexOf(w)].ingredients[ingredient]!! + 1
                        allergens[allergens.indexOf(w)].ingredients = allergens[allergens.indexOf(w)].ingredients
                    } else {
                        allergens[allergens.indexOf(w)].ingredients[ingredient] = 1
                    }
                }

            }

            foreignLabels.add(Pair(ingredientList, allergenList))

            ingredientList.forEach {
                if (it !in ingredientsToAllergens.keys) {
                    ingredientsToAllergens[it] = null
                }
            }
        }

        // Identify allergens until we all allergens are covered
        val allergensCount = allergens.map { it.name }.distinct().size
        while (ingredientsToAllergens.values.filterNotNull().size < allergensCount) {
            for (allergen in allergens) {
                for (ingredient in allergen.ingredients) {
                    val sortedIngredients = allergen.ingredients.toList().sortedByDescending { it.second }
                    val topWeightIngredient =
                        sortedIngredients.first().first == ingredient.key && (sortedIngredients.size == 1 || sortedIngredients[0].second > sortedIngredients[1].second)

                    // if this is the one with highest weight for this allergen
                    if (topWeightIngredient) {
                        ingredientsToAllergens[ingredient.key] = allergen.name

                        // Remove the allergen from the list to be processed and remove the ingredient to be considered
                        allergens = (allergens - allergen).map {
                            Allergen(
                                it.name,
                                // I couldn't figure out how to make simple assignment easier
                                ingredients = it.ingredients.filter { current -> current.key != ingredient.key } as MutableMap<String, Int>)
                        }.toMutableList()
                    }
                }
            }
        }

        return foreignLabels.map { label -> label.first.count { ingredientsToAllergens[it] == null } }.sum().toLong()
    }
}

fun main() {
    println(Day21.algo(Utils.getDayLines("21"))) // 2203
}
