package aoc2020

object Day24 {
    data class Tile(
        var white: Boolean = true,
        var i: Int,
        var j: Int,
        var e: Tile?,
        var se: Tile?,
        var sw: Tile?,
        var w: Tile?,
        var nw: Tile?,
        var ne: Tile?
    ) {
        operator fun get(str: String): Tile? = when (str) {
            "se" -> this.se
            "sw" -> this.sw
            "ne" -> this.ne
            "nw" -> this.nw
            "w" -> this.w
            "e" -> this.e
            else -> throw Exception("Invalid")
        }
    }

    fun algo(input: List<String>): Long {
        var board = mutableListOf<MutableList<Tile?>>()

        // Fill
        for (i in 0..999) {
            board.add(mutableListOf<Tile?>())
            for (j in 0..999) {
                board[i].add(j, Tile(e = null, se = null, sw = null, w = null, nw = null, ne = null, i = i, j = j))
            }
        }

        // Link
        for (i in 0..999) {
            for (j in 0..999) {
                if (i in 2..997 && j in 2..997) {
                    board[i][j] = board[i][j]?.apply {
                        e = board[i][j + 2]
                        se = board[i+1][j+1]
                        sw = board[i+1][j-1]
                        w = board[i][j - 2]
                        nw = board[i-1][j-1]
                        ne = board[i-1][j+1]
                    }
                }
            }
        }

        for (line in input) {
            var i = 499
            var j = 499
            var currentTile = board[i][j]
            var charPos = 0

            while (charPos < line.length) {
                if (charPos + 2 <= line.length && line.substring(charPos, charPos + 2) in listOf("se", "sw", "ne", "nw")) {
                    currentTile = currentTile?.get(line.substring(charPos, charPos + 2))
                    charPos += 2
                } else if (line.substring(charPos, charPos + 1) in listOf("e", "w")) {
                    currentTile = currentTile?.get(line.substring(charPos, charPos + 1))
                    charPos += 1
                } else {
                    throw Exception("what?")
                }
            }

            if (currentTile != null) {
                currentTile.white = !currentTile.white
            }
        }

        return board.map { row -> row.count { it != null && !it.white }.toLong() }.sum()
    }
}


fun main() {
    println(Day24.algo(Utils.getDayLines("24")))
}
