package aoc2020

object Day17 {
    fun algo(input: List<String>): Int {
        // I tweaked these numbers until the array was big enough to contain the full expansion
        val depth = 20
        var length = 100

        // If the input is an odd number make length odd number so the input can be placed in the center of the array
        if (input.size % 2 != 0) {
            length++
        }

        var arr = mutableListOf<MutableList<String>>()

        // Initialize array with max possible size for 6 cycles
        for (i in 0 until length) {
            var inner = mutableListOf<String>()
            for (j in 0 until length) {
                inner.add(".".repeat(length))
            }
            arr.add(inner)
        }

        // Place received input in the center
        for ((index, value) in input.withIndex()) {
            arr[depth / 2][(length / 2) - ((input.size / 2) - index + 1)] =
                ".".repeat((length / 2) - (value.length / 2)) + value + ".".repeat((length / 2) - (value.length / 2))
        }

        var arrCopy = arr.map { z -> z.map { y -> y }.map { it }.toMutableList() }.toMutableList()

        for (cycle in 1..6) {

            println("After $cycle cycles:")
            for ((z, _) in arr.withIndex()) {
                for ((y, _) in arr[z].withIndex()) {
                    for ((x, _) in arr[z][y].withIndex()) {

                        // The edges are not evaluated
                        if (z == 0 || z == arr.size - 1
                            || y == 0 || y == arr[z].size - 1
                            || x == 0 || x == arr[z][y].length - 1
                        ) {
                            continue
                        }

                        if (arr[z][y][x] == '#') {
                            applyActiveCubeRules(z, y, x, arr, arrCopy)
                        } else {
                            applyInactiveCubeRules(z, y, x, arr, arrCopy)
                        }
                    }


                }
            }

            arr = arrCopy.map { z -> z.map { y -> y }.map { it }.toMutableList() }.toMutableList()

            for ((z, _) in arr.withIndex()) {
                val relativeZ = (z) - (arr.size / 2)
                println("z=$relativeZ")
                for ((y, _) in arr[z].withIndex()) {
                    for ((x, _) in arr[z][y].withIndex()) {
                        print(arr[z][y][x])
                    }
                    println()
                }
            }

//            println("active cubes: ${countActiveCubes(arr)}")
        }

        return countActiveCubes(arr)

    }

    private fun countActiveCubes(arr: MutableList<MutableList<String>>): Int {
        var active = 0
        for ((z, _) in arr.withIndex()) {
            for ((y, _) in arr[z].withIndex()) {
                for ((x, _) in arr[z][y].withIndex()) {
                    if (arr[z][y][x] == '#') {
                        active++
                    }
                }
            }
        }
        return active
    }

    private fun applyActiveCubeRules(z: Int, y: Int, x: Int, arr: MutableList<MutableList<String>>, arrCopy: MutableList<MutableList<String>>) {
        val active = activeNeighbors(z, y, x, arr) - 1 //to compensate for the current active cube
        if (active != 2 && active != 3) {
            arrCopy[z][y] = arrCopy[z][y].replaceRange(x, x + 1, ".")
        }
    }

    private fun applyInactiveCubeRules(z: Int, y: Int, x: Int, arr: MutableList<MutableList<String>>, arrCopy: MutableList<MutableList<String>>) {
        if (activeNeighbors(z, y, x, arr) == 3) {
            arrCopy[z][y] = arrCopy[z][y].replaceRange(x, x + 1, "#")
        }
    }


    private fun activeNeighbors(z: Int, y: Int, x: Int, arr: MutableList<MutableList<String>>): Int {
        var active = 0

        for (zSubList in arr.subList(z - 1, z + 2)) {
            for (zySubList in zSubList.subList(y - 1, y + 2)) {
                for (cell in zySubList.substring(x - 1, x + 2)) {
                    if (cell == '#') {
                        active++
                    }
                }
            }
        }

        return active
    }

}

fun main() {
    println(Day17.algo(Utils.getDayLines("17")))// 271
}
